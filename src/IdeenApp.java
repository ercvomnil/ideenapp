import gui.IdeenFrame;

/**
 * App to collect and rate ideas.
 * 
 * @author Eric Rudolph
 */
public class IdeenApp {

	/**
	 * Progam entry.
	 * 
	 * @param args startup arguments-
	 * @author Eric Rudolph
	 */
	public static void main(String[] args) {

		final IdeenFrame frame = new IdeenFrame();
		frame.addNewIdeaFromInput("neu: Ganze tolle neue Idee!", false);
		frame.addNewIdeaFromInput("neu: Das ist aber interessant!", true);
		frame.addNewIdeaFromInput("neu: 42", false);

		// final Idee id1 = new Idee("Kaffee");
		// frame.addIdee(id1);
		// final IdeePMI id2 = new IdeePMI("Cola");
		// frame.addIdee(id2);
	}
}