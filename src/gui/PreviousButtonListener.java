package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PreviousButtonListener implements ActionListener {

	private final IdeenFrame	frame;

	public PreviousButtonListener(IdeenFrame frame) {
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.frame.previousIdea();
	}
}