package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Lists.SinglyLinkedList;

import components.Idee;
import components.IdeePMI;

import enums.UserInputCommands;

/**
 * The mein frame to show the app content.
 * @author Eric Rudolph
 */
/**
 *
 * @author Eric Rudolph
 */
/**
 *
 * @author Eric Rudolph
 */
/**
 * 
 * @author Eric Rudolph
 */
public class IdeenFrame extends JFrame {

	/**
	 * Random Serial Number to make eclipse happy.
	 * 
	 * @author Eric Rudolph
	 */
	private static final long				serialVersionUID	= 42L;

	/**
	 * The {@link JTextArea} to display the content of an idea.
	 * 
	 * @author Eric Rudolph
	 */
	private final JTextArea					ideaContentTextArea;

	/**
	 * The list containng the ideas.
	 * 
	 * @author Eric Rudolph
	 */
	private final SinglyLinkedList<Idee>	ideaList;

	/**
	 * The reference to the active idea.
	 * 
	 * @author Eric Rudolph
	 */
	private Idee							activeIdea;

	/**
	 * The {@link JLabel} to display the title of the idea.
	 * 
	 * @author Eric Rudolph
	 */
	private final JLabel					ideaTitle;

	/**
	 * The {@link JTextField} for user input.
	 * 
	 * @author Eric Rudolph
	 */
	private final JTextField				newArgumentField;

	/**
	 * The {@link JTextField} to enter a new idea.
	 * 
	 * @author Eric Rudolph
	 */
	private final JTextField				newIdeaField;

	/**
	 * The {@link JButton} to switch to the next {@link Idee}.
	 * 
	 * @author Eric Rudolph
	 */
	private final JButton					nextButton;

	/**
	 * The {@link JButton} to switch to the previous {@link Idee}.
	 * 
	 * @author Eric Rudolph
	 */
	private final JButton					previousButton;

	/**
	 * Ctor.
	 * 
	 * @author Eric Rudolph
	 */
	public IdeenFrame() {

		this.ideaList = new SinglyLinkedList<Idee>();
		this.activeIdea = null;

		// #################################################
		// # Settings
		// #################################################
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("IdeenApp");
		this.setAlwaysOnTop(true);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.setMinimumSize();

		// #################################################
		// # Top
		// #################################################
		final JPanel controlPanel = new JPanel();
		this.getContentPane().add(controlPanel, BorderLayout.NORTH);
		controlPanel.setLayout(new BorderLayout(0, 0));

		this.previousButton = new JButton("<");
		final PreviousButtonListener previousListener = new PreviousButtonListener(this);
		this.previousButton.addActionListener(previousListener);
		this.previousButton.setEnabled(false);
		controlPanel.add(this.previousButton, BorderLayout.WEST);

		this.nextButton = new JButton(">");
		final NextButtonListener nextListener = new NextButtonListener(this);
		this.nextButton.addActionListener(nextListener);
		this.nextButton.setEnabled(false);
		controlPanel.add(this.nextButton, BorderLayout.EAST);

		final JPanel titlePanel = new JPanel();
		controlPanel.add(titlePanel, BorderLayout.CENTER);

		this.ideaTitle = new JLabel("Neue Idee hinzufügen");
		this.ideaTitle.setFont(new Font("Arial", Font.BOLD, 14));
		titlePanel.add(this.ideaTitle);

		final AddIdeaButtonListener newIdeaListener = new AddIdeaButtonListener(this);
		this.newIdeaField = new JTextField();
		this.newIdeaField.addActionListener(newIdeaListener);

		final JPanel newIdeaPanel = new JPanel();
		newIdeaPanel.setLayout(new BoxLayout(newIdeaPanel, BoxLayout.X_AXIS));
		newIdeaPanel.add(this.newIdeaField);

		final JButton addIdeaButton = new JButton("Neu");
		addIdeaButton.addActionListener(newIdeaListener);
		newIdeaPanel.add(addIdeaButton);

		controlPanel.add(newIdeaPanel, BorderLayout.SOUTH);

		// #################################################
		// # Center
		// #################################################
		final JPanel ideaContentPanel = new JPanel();
		this.getContentPane().add(ideaContentPanel, BorderLayout.CENTER);
		ideaContentPanel.setLayout(new BorderLayout(0, 0));

		// Standard-Content
		final StringBuilder sb = new StringBuilder();
		sb.append(UserInputCommands.ADD_IDEA.getCommand());
		sb.append(" ");
		sb.append(UserInputCommands.ADD_IDEA.getDescription());
		sb.append("\r\n");
		sb.append(UserInputCommands.ADD_IDEA_PMI.getCommand());
		sb.append(" ");
		sb.append(UserInputCommands.ADD_IDEA_PMI.getDescription());

		this.ideaContentTextArea = new JTextArea(sb.toString());
		this.ideaContentTextArea.setMargin(new Insets(20, 20, 20, 20));

		ideaContentPanel.add(this.ideaContentTextArea);
		this.ideaContentTextArea.setEditable(false);

		// #################################################
		// # Bottom
		// #################################################
		final JPanel inputPanel = new JPanel();
		this.getContentPane().add(inputPanel, BorderLayout.SOUTH);
		final AddButtonListener addListener = new AddButtonListener(this);

		this.newArgumentField = new JTextField();
		this.newArgumentField.addActionListener(addListener);
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
		inputPanel.add(this.newArgumentField);
		this.newArgumentField.setColumns(2);

		final JButton addArgumentButton = new JButton("Add");
		addArgumentButton.addActionListener(addListener);
		inputPanel.add(addArgumentButton);

		// #################################################
		// # Draw Frame
		// #################################################
		this.setVisible(true);
		this.rescale();
		this.newArgumentField.requestFocus();
		this.setLocationRelativeTo(null);
	}

	/**
	 * Adds a new Idea to the List.
	 * 
	 * @author Eric Rudolph
	 */
	public void addIdee(Idee idee) {
		this.ideaList.addLast(idee);
		this.activeIdea = idee;
		this.updateContent();
	}

	/**
	 * Adds a new Idea to the list.
	 * 
	 * @param input the String containing the title.
	 * @param pmi if it should be a pmi
	 * @author Eric Rudolph
	 */
	public void addNewIdeaFromInput(String input, boolean pmi) {

		String substring;
		Idee tempIdea = null;

		if (pmi) {
			substring = input.substring(UserInputCommands.ADD_IDEA_PMI.getCommandLength());
			tempIdea = new IdeePMI(substring);
		} else {
			substring = input.substring(UserInputCommands.ADD_IDEA.getCommandLength());
			tempIdea = new Idee(substring);
		}

		if (tempIdea != null) {
			this.addIdee(tempIdea);
		}
	}

	/**
	 * Handles the inputed String.
	 * 
	 * @param input the input
	 * @author Eric Rudolph
	 */
	public void handleInput(String input) {
		if (this.ideaList.size() > 0) {
			this.activeIdea.addStichpunkt(input);
			this.updateContent();
		}
	}

	/**
	 * Handles the inputed String for a new idea.
	 * 
	 * @param input
	 * @author Eric Rudolph
	 */
	public void handleNewIdeaInput(String input) {
		final String lower = input.toLowerCase();

		if (lower.startsWith(UserInputCommands.ADD_IDEA_PMI.getLowerCommand())) {
			this.addNewIdeaFromInput(input, true);
		} else if (lower.startsWith(UserInputCommands.ADD_IDEA.getLowerCommand())) {
			this.addNewIdeaFromInput(input, false);
		}
	}

	/**
	 * Moves to the next idea.
	 * 
	 * @author Eric Rudolph
	 */
	public void nextIdea() {
		if (!this.ideaList.isEmpty()) {
			this.activeIdea = this.ideaList.getNext(this.activeIdea);
			this.updateContent();
		}
	}

	/**
	 * Moves to the previous idea.
	 * 
	 * @author Eric Rudolph
	 */
	public void previousIdea() {
		if (!this.ideaList.isEmpty()) {
			this.activeIdea = this.ideaList.getPrevious(this.activeIdea);
			this.updateContent();
		}
	}

	/**
	 * Rescales the window.
	 * 
	 * @author Eric Rudolph
	 */
	private void rescale() {
		this.setMinimumSize(this.getSize());
		this.pack();
		this.setMinimumSize();
	}

	/**
	 * Sets the minimum size for the window.
	 * 
	 * @author Eric Rudolph
	 */
	private void setMinimumSize() {
		this.setMinimumSize(new Dimension(300, 170));
	}

	/**
	 * Trigger the action of the submit button.
	 * 
	 * @author Eric Rudolph
	 */
	public void triggerAddButton() {
		final String text = this.newArgumentField.getText();
		this.handleInput(text);
		this.newArgumentField.setText("");
	}

	/**
	 * Trigger the action of the add idea button.
	 * 
	 * @author Eric Rudolph
	 */
	public void triggerAddIdeaButton() {
		final String text = this.newIdeaField.getText();
		this.handleNewIdeaInput(text);
		this.newIdeaField.setText("");
	}

	/**
	 * Adds an Idea to display.
	 * 
	 * @param idee the Idea to show the content.
	 * @author Eric Rudolph
	 */
	public void updateContent() {
		this.ideaTitle.setText(this.activeIdea.getTitle());
		this.ideaContentTextArea.setText(this.activeIdea.getContent());
		final boolean first = !this.ideaList.isFirst(this.activeIdea);
		final boolean last = !this.ideaList.isLast(this.activeIdea);
		this.previousButton.setEnabled(first);
		this.nextButton.setEnabled(last);

		this.rescale();
	}
}
