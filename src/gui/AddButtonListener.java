package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddButtonListener implements ActionListener {

	private final IdeenFrame	parentFrame;

	public AddButtonListener(IdeenFrame parentFrame) {
		this.parentFrame = parentFrame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.parentFrame.triggerAddButton();
	}
}