/**
 * 
 */
package Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import Lists.AbstractLinkedList;
import Lists.SinglyLinkedList;

import components.Idee;

/**
 * 
 * @author Eric Rudolph
 */
public class TestSinglyLinkedList {

	@Test
	public void testAddAt() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addAt(a, 0);
		assertEquals(l.getLast(), a);

		final Idee b = new Idee("B");
		l.addAt(b, 0);

		assertEquals(l.get(0), b);
		assertNotEquals(l.get(0), a);
		assertEquals(l.getLast(), a);

		final Idee c = new Idee("C");
		l.addAt(c, 0);
		assertEquals(l.get(0), c);
		assertEquals(l.getFirst(), c);
	}

	@Test
	public void testAddFirst() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);

		assertEquals(l.getFirst(), a);

		final Idee b = new Idee("B");
		l.addFirst(b);
		assertEquals(l.getFirst(), b);
		assertNotEquals(l.getFirst(), a);
	}

	@Test
	public void testAddLast() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addLast(a);

		assertEquals(l.getLast(), a);

		final Idee b = new Idee("B");
		l.addLast(b);
		assertEquals(l.getLast(), b);
		assertNotEquals(l.getLast(), a);
	}

	@Test
	public void testContains() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);
		final Idee b = new Idee("B");
		l.addLast(b);

		final Idee c = new Idee("C");

		assertEquals(Boolean.valueOf(l.contains(a)), Boolean.TRUE);
		assertEquals(Boolean.valueOf(l.contains(b)), Boolean.TRUE);
		assertNotEquals(Boolean.valueOf(l.contains(c)), Boolean.TRUE);
	}

	@Test
	public void testFirstLast() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);
		final Idee b = new Idee("B");
		l.addLast(b);

		assertEquals(Boolean.valueOf(l.isFirst(a)), Boolean.TRUE);
		assertEquals(Boolean.valueOf(l.isFirst(b)), Boolean.FALSE);
		assertEquals(Boolean.valueOf(l.isLast(a)), Boolean.FALSE);
		assertEquals(Boolean.valueOf(l.isLast(b)), Boolean.TRUE);
	}

	@Test
	public void testGetNext() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);
		final Idee b = new Idee("B");
		l.addLast(b);

		assertEquals(l.getNext(a), b);
		assertEquals(l.getNext(b), b);
	}

	@Test
	public void testGetPrevious() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);
		final Idee b = new Idee("B");
		l.addLast(b);

		assertEquals(l.getPrevious(a), a);
		assertEquals(l.getPrevious(b), a);
	}

	@Test
	public void testIsEmpty() {
		final AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		assertEquals(Boolean.valueOf(l.isEmpty()), Boolean.TRUE);

		final Idee a = new Idee("A");
		l.addFirst(a);

		assertEquals(Boolean.valueOf(l.isEmpty()), Boolean.FALSE);
	}

	@Test
	public void testRemove() {
		AbstractLinkedList<Idee> l = new SinglyLinkedList<Idee>();

		final Idee a = new Idee("A");
		l.addFirst(a);
		final Idee b = new Idee("B");
		l.addLast(b);
		final Idee c = new Idee("C");

		l.removeObject(a);

		assertEquals(l.getFirst(), b);
		assertEquals(l.getLast(), b);

		l = new SinglyLinkedList<Idee>();
		l.addFirst(a);
		l.addLast(b);
		l.addLast(c);

		assertEquals(l.getNext(a), b);
		assertEquals(l.getNext(b), c);
		assertEquals(l.getPrevious(c), b);

		l.removeObject(b);

		assertEquals(l.getNext(a), c);
		assertEquals(l.getPrevious(c), a);

		assertEquals(Boolean.valueOf(l.contains(a)), Boolean.TRUE);
		assertEquals(Boolean.valueOf(l.contains(b)), Boolean.FALSE);
		assertEquals(Boolean.valueOf(l.contains(c)), Boolean.TRUE);
	}
}