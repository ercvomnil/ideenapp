package Lists;

import java.util.NoSuchElementException;

/**
 * Linked List to Save Data.
 * 
 * @param <E> the Type to be saved in the List.
 * @author Eric Rudolph
 */
public class SinglyLinkedList<E> implements AbstractLinkedList<E> {

	/**
	 * The class to describe the Nodes saved in the list.
	 * 
	 * @author Eric Rudolph
	 */
	private class Node {
		/**
		 * The data contained in the node.
		 * 
		 * @author Eric Rudolph
		 */
		private final E	data;

		/**
		 * The link to the next node.
		 * 
		 * @author Eric Rudolph
		 */
		private Node	next;

		/**
		 * ctor.
		 * 
		 * @param e the data.
		 * @param next the link.
		 * @author Eric Rudolph
		 */
		public Node(E e, Node next) {
			this.data = e;
			this.next = next;
		}
	}

	/**
	 * The first node to start from.
	 * 
	 * @author Eric Rudolph
	 */
	private Node	first	= null;

	/**
	 * The size of the list.
	 * 
	 * @author Eric Rudolph
	 */
	private int		size	= 0;

	@Override
	public void addAt(E e, int i) {

		if (i == 0 || this.isEmpty()) {
			// Empty of index = 0 => first element
			this.addFirst(e);
		} else if (i >= this.size) {
			// index greater or equal size => last element
			this.addLast(e);
		} else {
			// normal add at index
			Node tmp = this.first;
			Node last = tmp;
			int cnt = 0;

			while (tmp != null) {
				if (cnt == i) {
					final Node newNode = new Node(e, tmp);
					last.next = newNode;
					this.size++;
					break;
				}

				cnt++;
				last = tmp;
				tmp = tmp.next;
			}
		}
	}

	@Override
	public void addFirst(E e) {
		final Node node = new Node(e, this.first);
		this.first = node;
		this.size++;
	}

	@Override
	public void addLast(E e) {
		if (!this.isEmpty()) {
			final Node last = this.getLastNode();
			final Node newNode = new Node(e, null);
			last.next = newNode;
			this.size++;
		} else {
			this.addFirst(e);
		}
	}

	/**
	 * Checks if the list is empty and throws {@link NoSuchElementException} if so.
	 * 
	 * @throws NoSuchElementException
	 * @author Eric Rudolph
	 */
	private void checkEmpty() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}
	}

	@Override
	public boolean contains(E e) {
		if (this.isEmpty()) {
			return false;
		} else {

			Node tmp = this.first;

			while (tmp != null) {
				if (tmp.data != null && tmp.data == e) {
					return true;
				}

				tmp = tmp.next;
			}

			return false;
		}
	}

	@Override
	public E get(int i) throws NoSuchElementException {
		this.checkEmpty();

		int pos = 0;

		Node tmp = this.first;
		E res = null;

		while (tmp != null) {
			if (pos == i) {
				res = tmp.data;
			}

			pos++;
			tmp = tmp.next;
		}

		if (res == null) {
			throw new NoSuchElementException();
		}

		return res;
	}

	@Override
	public E getFirst() throws NoSuchElementException {
		this.checkEmpty();
		return this.first.data;
	}

	@Override
	public E getLast() throws NoSuchElementException {
		this.checkEmpty();

		final Node tmp = this.getLastNode();
		return tmp.data;
	}

	/**
	 * Gets the last {@link Node}.
	 * 
	 * @return the node.
	 * @author Eric Rudolph
	 */
	private Node getLastNode() {
		Node tmp = this.first;

		while (tmp.next != null) {
			tmp = tmp.next;
		}

		return tmp;
	}

	@Override
	public E getNext(E e) {
		Node tmp = this.first;
		Node target = null;

		while (tmp != null) {
			if (tmp.data != null && tmp.data == e) {
				target = tmp.next;
				break;
			}

			tmp = tmp.next;
		}

		return target != null ? target.data : e;
	}

	@Override
	public E getPrevious(E e) {
		Node tmp = this.first;
		Node last = this.first;

		while (tmp != null) {
			if (tmp.data != null && tmp.data == e) {
				break;
			}

			last = tmp;
			tmp = tmp.next;
		}

		return last != null ? last.data : e;
	}

	@Override
	public boolean isEmpty() {
		return this.first == null;
	}

	@Override
	public boolean isFirst(E e) {
		if (this.isEmpty()) {
			return false;
		}
		return e == this.first.data;
	}

	@Override
	public boolean isLast(E e) {
		if (this.isEmpty()) {
			return false;
		}

		final Node last = this.getLastNode();
		return last.data == e;
	}

	@Override
	public void removeObject(E e) {
		this.checkEmpty();

		Node tmp = this.first;
		Node last = tmp;

		while (tmp != null) {
			if (tmp.data == e) {
				last.next = tmp.next;

				if (tmp == this.first) {
					this.first = this.first.next;
				}
			}

			last = tmp;
			tmp = tmp.next;
		}
	}

	@Override
	public int size() {
		return this.size;
	}
}