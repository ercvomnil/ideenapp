package Lists;

import java.util.NoSuchElementException;

/**
 * The interface of an abstract linked list.
 * 
 * @param <E> the data type.
 * @author Eric Rudolph
 */
public interface AbstractLinkedList<E> {

	/**
	 * Adds an Object of Type E at the Position i.
	 * 
	 * @param e the data.
	 * @param i the index.
	 * @author Eric Rudolph
	 */
	public abstract void addAt(E e, int i);

	/**
	 * Adds a new element at the start of the list.
	 * 
	 * @param e the object to be added.
	 * @author Eric Rudolph
	 */
	public abstract void addFirst(E e);

	/**
	 * Adds a new element at the end of the list.
	 * 
	 * @param e the object to be added.
	 * @author Eric Rudolph
	 */
	public abstract void addLast(E e);

	/**
	 * Checks if the list contains element of type E.
	 * 
	 * @param e the Element to be searched.
	 * @return true if found.
	 * @author Eric Rudolph
	 */
	public abstract boolean contains(E e);

	/**
	 * Gets the element at position i if found.
	 * 
	 * @param i the index
	 * @return the element
	 * @throws NoSuchElementException
	 * @author Eric Rudolph
	 */
	public abstract E get(int i) throws NoSuchElementException;

	/**
	 * Gets the first element of the list if found.
	 * 
	 * @return the element.
	 * @throws NoSuchElementException
	 * @author Eric Rudolph
	 */
	public abstract E getFirst() throws NoSuchElementException;

	/**
	 * Gets the last element of the list if found.
	 * 
	 * @return the element.
	 * @throws NoSuchElementException
	 * @author Eric Rudolph
	 */
	public abstract E getLast() throws NoSuchElementException;

	/**
	 * Gets the data of the next node if existent, else returns entered data.
	 * 
	 * @param e the current data.
	 * @return the next data.
	 * @author Eric Rudolph
	 */
	public abstract E getNext(E e);

	/**
	 * Gets the data of the previous node if existent, else returns entered data.
	 * 
	 * @param e the current data.
	 * @return the previous data.
	 * @author Eric Rudolph
	 */
	public abstract E getPrevious(E e);

	/**
	 * Returns true if the list is empty.
	 * 
	 * @return true on empty.
	 * @author Eric Rudolph
	 */
	public abstract boolean isEmpty();

	/**
	 * Returns true if the Data e is the first element in the list.
	 * 
	 * @param e the data
	 * @return true if e is the first.
	 * @author Eric Rudolph
	 */
	public boolean isFirst(E e);

	/**
	 * Returns true if the data e is the last element in the list.
	 * 
	 * @param e the data
	 * @return true if the last.
	 * @author Eric Rudolph
	 */
	public boolean isLast(E e);

	/**
	 * Removes the Object e from the list, if found.
	 * 
	 * @param e the data to be removed.
	 * @author Eric Rudolph
	 */
	public abstract void removeObject(E e);

	/**
	 * Returns the size of the list.
	 * 
	 * @return the size.
	 * @author Eric Rudolph
	 */
	public abstract int size();
}