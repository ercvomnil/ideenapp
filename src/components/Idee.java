package components;

import enums.ListType;

/**
 * Describes an idea with it's title, pro and contra arguments.
 * @author Eric Rudolph
 */
/**
 *
 * @author Eric Rudolph
 */
public class Idee {

	/**
	 * The list containing the contra arguments.
	 * @author Eric Rudolph
	 */
	private final Stichpunktliste	contra;

	/**
	 * The list containing the pro arguments.
	 * @author Eric Rudolph
	 */
	private final Stichpunktliste	pro;

	/**
	 * The title of the idea.
	 * @author Eric Rudolph
	 */
	private final String			title;

	/**
	 * Ctor.
	 * @author Eric Rudolph
	 */
	public Idee(String title) {
		this.title = title.trim();

		this.pro = new Stichpunktliste(ListType.PRO);
		this.contra = new Stichpunktliste(ListType.CONTRA);
	}

	/**
	 * Adds an item to an argument list.
	 * @param stichpunkt the String containing the arguments type and title.
	 * @author Eric Rudolph
	 */
	public void addStichpunkt(String stichpunkt) {
		String substring;
		final String lower = stichpunkt.toLowerCase();

		if (lower.startsWith(ListType.PRO.getAddCommand())) {

			substring = stichpunkt.substring(ListType.PRO.getCommandLength());
			this.pro.addStichpunkt(substring);

		} else if (lower.startsWith(ListType.CONTRA.getAddCommand())) {

			substring = stichpunkt.substring(ListType.CONTRA.getCommandLength());
			this.contra.addStichpunkt(substring);

		}
	}

	/**
	 * Gets the contra list.
	 * @return the contra list
	 * @author Eric Rudolph
	 */
	protected Stichpunktliste getContra() {
		return this.contra;
	}

	/**
	 * Returns the pro list.
	 * @return the pro list
	 * @author Eric Rudolph
	 */
	protected Stichpunktliste getPro() {
		return this.pro;
	}

	/**
	 * Getter for the title.
	 * @return the title
	 * @author Eric Rudolph
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Returns the content of the idea as String.
	 * @return the content.
	 * @author Eric Rudolph
	 */
	public String getContent() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getListContents());
		sb.append(this.getTrend());
		return sb.toString();
	}

	/**
	 * Returns the Trend of the idea as String.
	 * @return the trend.
	 * @author Eric Rudolph
	 */
	private String getTrend() {
		if (this.pro.getCounter() > this.contra.getCounter()) {
			return "Tendenz: Positiv";
		} else if (this.pro.getCounter() == this.contra.getCounter()) {
			return "Tendenz: Neutral";
		} else {
			return "Tendenz: Negativ";
		}
	}

	/**
	 * Retunrs the content of the Lists as String.
	 * @return
	 * @author Eric Rudolph
	 */
	protected String getListContents() {
		final StringBuilder sb = new StringBuilder();

		final String proContent = this.getListContent(this.pro);
		final String contraContent = this.getListContent(this.contra);

		sb.append(proContent);
		sb.append(contraContent);

		return sb.toString();
	}

	/**
	 * Returns the content of a {@link Stichpunktliste}.
	 * @param list
	 * @return
	 * @author Eric Rudolph
	 */
	protected String getListContent(Stichpunktliste list) {
		final StringBuilder sb = new StringBuilder();

		sb.append(list.getListContent());
		sb.append("\r\n");

		return sb.toString();
	}
}
