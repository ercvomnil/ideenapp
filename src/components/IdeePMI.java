/**
 * FILES!
 */
package components;

import enums.ListType;

/**
 *
 * @author Eric Rudolph
 */
public class IdeePMI extends Idee {

	/**
	 * The list containing interesting arguments.
	 * @author Eric Rudolph
	 */
	private final Stichpunktliste	interesting;

	/**
	 * Ctor.
	 * @author Eric Rudolph
	 */
	public IdeePMI(String title) {
		super(title);

		this.interesting = new Stichpunktliste(ListType.INTERESTING);
	}

	@Override
	public void addStichpunkt(String stichpunkt) {
		super.addStichpunkt(stichpunkt);

		final String lower = stichpunkt.toLowerCase();

		if (lower.startsWith(ListType.INTERESTING.getAddCommand())) {

			final String substring = stichpunkt.substring(ListType.INTERESTING.getCommandLength());
			this.interesting.addStichpunkt(substring);
		}
	}

	@Override
	protected String getListContents() {
		final StringBuilder sb = new StringBuilder();

		sb.append(super.getListContents());
		final String interestingContent = this.getListContent(this.interesting);
		sb.append(interestingContent);

		return sb.toString();
	}
}
