package components;

import enums.ListType;

/**
 * The list containing arguments.
 * @author Eric Rudolph
 */
public class Stichpunktliste {

	/**
	 * Defines how many items can be saved.
	 * @author Eric Rudolph
	 */
	public static final int	MAX_LIST_ITEMS	= 20;

	/**
	 * The {@link ListType} of the list.
	 * @author Eric Rudolph
	 */
	private final ListType	type;

	/**
	 * The array containing the items.
	 * @author Eric Rudolph
	 */
	private final String[]	listItems;

	/**
	 * A number how many items have been saved.
	 * @author Eric Rudolph
	 */
	private int				counter;

	/**
	 * Ctor.
	 * @author Eric Rudolph
	 */
	public Stichpunktliste(ListType type) {
		this.type = type;
		this.counter = 0;
		this.listItems = new String[MAX_LIST_ITEMS];
	}

	/**
	 * Adds a new item to the list.
	 * @param s the title of the new item.
	 * @author Eric Rudolph
	 */
	public void addStichpunkt(String s) {
		if (this.counter < MAX_LIST_ITEMS) {
			if (!s.trim().equals("")) {
				this.listItems[this.counter] = s.trim();
				this.counter += 1;
			}
		} else {
			System.err.println("[Fehler] Maximale Anzahl an Stichpunkten ist erreicht.");
		}
	}

	/**
	 * Returns the content of the list.
	 * @return the content.
	 * @author Eric Rudolph
	 */
	public String getListContent() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.type.getTitle());
		sb.append(" (");
		sb.append(this.counter);
		sb.append("/");
		sb.append(MAX_LIST_ITEMS);
		sb.append(") :");

		for (int i = 0; i < this.counter; i++) {
			sb.append("\r\n");
			sb.append("     ");
			sb.append("- ");
			sb.append(this.listItems[i]);
		}

		if (this.counter > 0) {
			sb.append("\r\n");
		}

		return sb.toString();
	}

	/**
	 * Returns the title of the last saved item.
	 * @return the title.
	 * @author Eric Rudolph
	 */
	public String getLastItem() {
		return this.counter > 0 ? this.listItems[this.counter - 1] : "";
	}

	/**
	 * Returns the number of list elements.
	 * @return the counter
	 * @author Eric Rudolph
	 */
	public int getCounter() {
		return this.counter;
	}

	/**
	 * Returns the list type.
	 * @return the type
	 * @author Eric Rudolph
	 */
	public ListType getType() {
		return this.type;
	}
}
