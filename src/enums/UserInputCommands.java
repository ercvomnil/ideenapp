package enums;

/**
 * Contains the commands a user can enter.
 * @author Eric Rudolph
 */
public enum UserInputCommands {
	ADD_IDEA("neu:", "Neue Idee anlegen"),
	ADD_IDEA_PMI("neui:", "Neue Idee mit Interessant-Liste anlegen"),
	CHOOSE_IDEA("aktiv:", "Aktive Idee ausw�hlen"),
	SHOW_IDEAS("zeige ideen", "Liste aller Ideen anzeigen"),
	HELP("hilfe", "Hilfe"),
	EXIT("exit", "Programm beenden");

	/**
	 * Contains the command.
	 * @author Eric Rudolph
	 */
	private String	command;

	/**
	 * Contains the description of the command.
	 * @author Eric Rudolph
	 */
	private String	description;

	/**
	 * Ctor.
	 * @author Eric Rudolph
	 */
	private UserInputCommands(String command, String desc) {
		this.command = command;
		this.description = desc;
	}

	/**
	 * Returns the description of a command.
	 * @return the description
	 * @author Eric Rudolph
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Returns the command-string.
	 * @return the string
	 * @author Eric Rudolph
	 */
	public String getCommand() {
		return this.command;
	}

	/**
	 * Returns the command-string.
	 * @return the string
	 * @author Eric Rudolph
	 */
	public String getLowerCommand() {
		return this.command.toLowerCase();
	}

	/**
	 * Returns the length of a command.
	 * @return the length
	 * @author Eric Rudolph
	 */
	public int getCommandLength() {
		return this.command.length();
	}
}
