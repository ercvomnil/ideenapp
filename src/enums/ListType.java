package enums;

/**
 * Contains the types, a {@link Stichpunktlist} can assume.
 *
 * @author Eric Rudolph
 */
public enum ListType {

	PRO("Pro", "pro:"),
	CONTRA("Kontra", "kontra:"),
	INTERESTING("Interessant", "int:");

	/**
	 * The title of the type.
	 * @author Eric Rudolph
	 */
	private final String	title;

	/**
	 * The String to be checked against on input of new items.
	 * @author Eric Rudolph
	 */
	private final String	addCommand;

	/**
	 * Ctor.
	 * @author Eric Rudolph
	 */
	private ListType(String title, String addCommmand) {
		this.title = title;
		this.addCommand = addCommmand;
	}

	/**
	 * Returns the title of the ListType.
	 * @return the title.
	 * @author Eric Rudolph
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Returns the String to check on input.
	 * @return the addCommand.
	 * @author Eric Rudolph
	 */
	public String getAddCommand() {
		return this.addCommand;
	}

	/**
	 * Returns the length of the command String for substrings.
	 * @return the length.
	 * @author Eric Rudolph
	 */
	public int getCommandLength() {
		return this.addCommand.length();
	}
}
